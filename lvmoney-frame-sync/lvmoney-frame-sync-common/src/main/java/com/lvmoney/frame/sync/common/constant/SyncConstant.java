package com.lvmoney.frame.sync.common.constant;/**
 * 描述:
 * 包名:com.lvmoney.frame.sync.common.constant
 * 版本信息: 版本1.0
 * 日期:2022/1/6
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/1/6 11:07
 */
public class SyncConstant {
    /**
     * redis ssync 分组
     */
    public static final String REDIS_SYNC_GROUP = "SYNC:";
}
